require 'rails_helper'

RSpec.describe "games/show", type: :view do
  before(:each) do
    @game = assign(:game, Game.create!(
      :game_type => "Game Type",
      :team_a => 2,
      :team_b => 3
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Game Type/)
    expect(rendered).to match(/2/)
    expect(rendered).to match(/3/)
  end
end
