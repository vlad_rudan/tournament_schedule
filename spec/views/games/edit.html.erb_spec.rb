require 'rails_helper'

RSpec.describe "games/edit", type: :view do
  before(:each) do
    @game = assign(:game, Game.create!(
      :game_type => "MyString",
      :team_a => 1,
      :team_b => 1
    ))
  end

  it "renders the edit game form" do
    render

    assert_select "form[action=?][method=?]", game_path(@game), "post" do

      assert_select "input[name=?]", "game[game_type]"

      assert_select "input[name=?]", "game[team_a]"

      assert_select "input[name=?]", "game[team_b]"
    end
  end
end
