require 'rails_helper'

RSpec.describe "games/index", type: :view do
  before(:each) do
    assign(:games, [
      Game.create!(
        :game_type => "Game Type",
        :team_a => 2,
        :team_b => 3
      ),
      Game.create!(
        :game_type => "Game Type",
        :team_a => 2,
        :team_b => 3
      )
    ])
  end

  it "renders a list of games" do
    render
    assert_select "tr>td", :text => "Game Type".to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => 3.to_s, :count => 2
  end
end
