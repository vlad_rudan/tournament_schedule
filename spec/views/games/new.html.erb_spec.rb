require 'rails_helper'

RSpec.describe "games/new", type: :view do
  before(:each) do
    assign(:game, Game.new(
      :game_type => "MyString",
      :team_a => 1,
      :team_b => 1
    ))
  end

  it "renders new game form" do
    render

    assert_select "form[action=?][method=?]", games_path, "post" do

      assert_select "input[name=?]", "game[game_type]"

      assert_select "input[name=?]", "game[team_a]"

      assert_select "input[name=?]", "game[team_b]"
    end
  end
end
