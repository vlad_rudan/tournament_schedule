require 'rails_helper'

RSpec.describe "scores/edit", type: :view do
  before(:each) do
    @score = assign(:score, Score.create!(
      :team_id => 1,
      :game_id => 1,
      :score => "MyString",
      :point => 1
    ))
  end

  it "renders the edit score form" do
    render

    assert_select "form[action=?][method=?]", score_path(@score), "post" do

      assert_select "input[name=?]", "score[team_id]"

      assert_select "input[name=?]", "score[game_id]"

      assert_select "input[name=?]", "score[score]"

      assert_select "input[name=?]", "score[point]"
    end
  end
end
