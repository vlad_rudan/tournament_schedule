require 'rails_helper'

RSpec.describe "scores/index", type: :view do
  before(:each) do
    assign(:scores, [
      Score.create!(
        :team_id => 2,
        :game_id => 3,
        :score => "Score",
        :point => 4
      ),
      Score.create!(
        :team_id => 2,
        :game_id => 3,
        :score => "Score",
        :point => 4
      )
    ])
  end

  it "renders a list of scores" do
    render
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => 3.to_s, :count => 2
    assert_select "tr>td", :text => "Score".to_s, :count => 2
    assert_select "tr>td", :text => 4.to_s, :count => 2
  end
end
