class Game < ApplicationRecord
  
  scope :division_a, -> { where(division: 'A') }
  scope :division_b, -> { where(division: 'B') }
  scope :play_off, -> { where(division: 'P') }
  
  has_many :scores
  
  belongs_to :first_team, class_name: 'Team', foreign_key: "team_a"
  belongs_to :second_team, class_name: 'Team', foreign_key: "team_b"
 
  
end
