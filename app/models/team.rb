class Team < ApplicationRecord
  has_many :scores

  scope :division_a, -> { where(division: 'A') }
  scope :division_b, -> { where(division: 'B') }

  #attr_accessor :division_game_points
    
  def division_games(game_type)
    Game.where("(team_a = ? or team_b = ?) and game_type = ?", id, id, game_type)
  end
  
  def division_games_results(game_type)
    division_games(game_type).map{|game| game.scores}.flatten 
  end
  
  def division_games_points(game_type)
    division_games_results(game_type).map{|score| score.point if score.team_id == id}.compact 
  end
  
  def division_game_result(division, team_a, team_b)
    if team_a == team_b
      return "- : -"
    else
      game = Game.where("game_type = ? and ( (team_a = ? and team_b = ?) or (team_a = ? and team_b = ?) )", division, team_a, team_b, team_b, team_a).first
      if game
        score = Score.where(game_id: game.id, team_id: team_a).first
        if score
          return score.score
        end
      else 
        retun ' : '
      end
      
    end
  end

  
end
