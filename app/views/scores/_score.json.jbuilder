json.extract! score, :id, :team_id, :game_id, :score, :point, :created_at, :updated_at
json.url score_url(score, format: :json)
