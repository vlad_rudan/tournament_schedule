json.extract! game, :id, :game_type, :team_a, :team_b, :created_at, :updated_at
json.url game_url(game, format: :json)
