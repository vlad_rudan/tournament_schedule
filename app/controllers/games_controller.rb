class GamesController < ApplicationController
  before_action :set_game, only: [:show, :edit, :update, :destroy]

  # GET /games
  # GET /games.json
  def index
    @a_division_teams = Team.division_a
    @b_division_teams = Team.division_b
    
    @best_a_division_teams = get_team_list_ordered_by_result(@a_division_teams, 'A')
    @best_b_division_teams = get_team_list_ordered_by_result(@b_division_teams, 'B')

#    @best_all_teams << @best_a_division_teams 
#    @best_all_teams << @best_b_division_teams
  end

   def generate_result
    division = params[:division]
    games = Game.where(game_type: division)
    
    games.each { |game|
      if !game.scores.exists?
        a_game_point = rand(0 .. 9)
        b_game_point = rand(0 .. 9)
        Score.create(team_id: game.team_a, game_id: game.id, score: "#{a_game_point} : #{b_game_point}" , point: a_game_point == b_game_point ? 1 : a_game_point > b_game_point ? 3 : 0)
        Score.create(team_id: game.team_b, game_id: game.id, score: "#{b_game_point} : #{a_game_point}", point: a_game_point == b_game_point ? 1 : b_game_point > a_game_point ? 3 : 0)      
      end 
          
    } 
    
    respond_to do |format|
      format.html { redirect_to games_url, notice: "Results for division #{division} successfully generated." }
      format.json { head :no_content }
    end
    
  end
  
   def generate_play_off_games_with_results
     @best_division_teams = @best_a_division_teams + @best_b_division_teams
     @best_division_teams = @best_division_teams.sort{|a, b| a.division_games_points(a.game_type).sum <=> b.division_games_points(b.game_type).sum }.reverse!
      Rails.logger.info  @best_division_teams.inspect
#     while(@best_division_teams.size > 0)
#      Game.create(game_type: 'P', team_a: @best_a_division_teams.shift.id, team_b: @best_a_division_teams.shift.id) 
#      Game.create(game_type: 'P', team_a: @best_a_division_teams.shift.id, team_b: @best_a_division_teams.pop.id) 
#     end
   end
  
   private 
   
   def get_team_list_ordered_by_result(division_teams, game_type)
    ordered_division_teams = division_teams.sort {|a, b| a.division_games_points(game_type).sum <=> b.division_games_points(game_type).sum }.reverse!
    return ordered_division_teams.take(4)
     
   end
 
end
