Rails.application.routes.draw do
  root 'games#index'
  resources :scores
  resources :teams

  resources :games do
    post 'generate_result', on: :collection
    post 'generate_play_off_games_with_results', on: :collection
  end
  
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
