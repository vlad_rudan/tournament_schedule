class AddInitialTeams < ActiveRecord::Migration[5.1]
  def up
    8.times do |i|
      Team.create(name: "Team #A#{i + 1}", division: "A")
    end

    8.times do |i|
      Team.create(name: "Team #B#{i + 1}", division: "B")
    end
	
  end

  def down
    Team.delete_all
  end
  
end
