class AddInitialGames < ActiveRecord::Migration[5.1]
  def up
    create_division_games(Team.division_a)
    create_division_games(Team.division_b)
  end

  def down
    Game.delete_all
  end

  def create_division_games(division_teams)
    division_size = division_teams.size
    division_size.times do |i|
        (division_size - (i + 1)).times do |k|
          Game.create(game_type: division_teams[i].division, team_a: division_teams[i].id, team_b: division_teams[k + i + 1].id) 
        end
    end
  end
end
