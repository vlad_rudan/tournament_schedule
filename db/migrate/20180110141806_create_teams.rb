class CreateTeams < ActiveRecord::Migration[5.1]
  def change
    create_table :teams do |t|
      t.text :name
      t.text :division

      t.timestamps
    end
  end
end
