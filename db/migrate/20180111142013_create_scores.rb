class CreateScores < ActiveRecord::Migration[5.1]
  def change
    create_table :scores do |t|
      t.integer :team_id
      t.integer :game_id
      t.string :score
      t.integer :point

      t.timestamps
    end
  end
end
