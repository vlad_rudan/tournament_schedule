class CreateGames < ActiveRecord::Migration[5.1]
  def change
    create_table :games do |t|
      t.string :game_type
      t.integer :team_a
      t.integer :team_b

      t.timestamps
    end
  end
end
